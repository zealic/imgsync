ARG ALPINE_VER=3.10
################################################################################
# Source
################################################################################
FROM golang:1-alpine AS source

RUN export DEPS=" \
    curl tar musl-dev make libtool" \
    && apk add $DEPS
ENV LSTAGS_VER=push-template
ENV LSTAGS_URL=https://github.com/zealic/lstags/archive/${LSTAGS_VER}.tar.gz
ENV LSTAGS_DIR=/lstags

RUN mkdir $LSTAGS_DIR
WORKDIR $LSTAGS_DIR
RUN curl -sSL $LSTAGS_URL | tar --strip-components=1 -C $LSTAGS_DIR -xvzf -
RUN go build -mod=vendor -o lstags
RUN mv lstags /usr/local/bin/


################################################################################
# Runtime
################################################################################
FROM alpine:$ALPINE_VER
RUN export DEPS=" \
    ca-certificates" \
    && apk add $DEPS
COPY --from=source /usr/local/bin/* /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/lstags"]
